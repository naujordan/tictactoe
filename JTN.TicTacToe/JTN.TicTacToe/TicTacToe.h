#pragma once
#include <iostream>

class TicTacToe
{
	//Private fields used
	private:

		//numTurns is used to track if the game is going to end in a tie
		int m_numTurns = 9;
		//Starts with Player X, then will change depending on the turn
		char m_playerTurn = 'X';
		//Will be used to display the winner
		char m_winner = ' ';
		//Holds temp values for the board
		char m_board[9] = {'1', '2', '3', '4', '5', '6','7', '8', '9'};


	public:
		//METHODS

		//Outputs the board to the screen
		void DisplayBoard() 
		{
			std::cout << m_board[0] << " | " << m_board[1] << " | " << m_board[2] << "\n";
			std::cout << "---------" << "\n";
			std::cout << m_board[3] << " | " << m_board[4] << " | " << m_board[5] << "\n";
			std::cout << "---------" << "\n";
			std::cout << m_board[6] << " | " << m_board[7] << " | " << m_board[8] << "\n";
		};

		//Checks for a win condition
		bool IsOver()
		{
			//Top row win
			if ((m_board[0] == m_board[1]) && (m_board[1] == m_board[2]))
			{
				m_winner = m_board[0];
				return true;
			}
			//middle row win
			else if ((m_board[3] == m_board[4]) && (m_board[4] == m_board[5]))
			{
				m_winner = m_board[3];
				return true;
			}
			//bottom row win
			else if ((m_board[6] == m_board[7]) && (m_board[7] == m_board[8]))
			{
				m_winner = m_board[6];
				return true;
			}
			//first column win
			else if ((m_board[0] == m_board[3]) && (m_board[3] == m_board[6]))
			{
				m_winner = m_board[0];
				return true;
			}
			//second column win
			else if ((m_board[1] == m_board[4]) && (m_board[4] == m_board[7]))
			{
				m_winner = m_board[1];
				return true;
			}
			//third column win
			else if ((m_board[2] == m_board[5]) && (m_board[5] == m_board[8]))
			{
				m_winner = m_board[2];
				return true;
			}
			//Diagonal win
			else if ((m_board[0] == m_board[4]) && (m_board[4] == m_board[8]))
			{
				m_winner = m_board[0];
				return true;
			}
			//other diagonal win
			else if ((m_board[2] == m_board[4]) && (m_board[4] == m_board[6]))
			{
				m_winner = m_board[2];
				return true;
			}
			//All spots filled with no winner
			else if (m_numTurns == 0)
			{
				return true;
			}
			//No win condition/not all spots filled
			else
			{
				return false;
			}
		};

		//Used to display the players turn
		char GetPlayerTurn()
		{
			if (m_playerTurn == 'X')
				return 'X';
			else
				return 'O';
		};

		//Checks the array to see if the position has already been chosen
		bool IsValidMove(int position)
		{
			if (m_board[position - 1] == 'X' || m_board[position - 1] == 'O')
				return false;
			else
				return true;
		};

		//If the move is valid, sets X or O then switches the players turn
		void Move(int position)
		{
			m_board[position - 1] = m_playerTurn;
			m_numTurns--;
			if (m_playerTurn == 'X')
				m_playerTurn = 'O';
			else
				m_playerTurn = 'X';
		};
		//Displays winner
		void DisplayResult()
		{
			if (m_winner == ' ')
				std::cout << "The result is a tie!" << "\n";
			else
				std::cout << "The winner is " << m_winner << "!" << "\n";
		};

};